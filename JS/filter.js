export function filter() {
  // filter by search
  document.addEventListener("DOMContentLoaded", function () {
    const searchInput = document.getElementById("searchInput")
    const headings = document.querySelectorAll(".card h3")

    searchInput.addEventListener("input", function () {
      const searchText = this.value.trim().toLowerCase()

      headings.forEach(function (heading) {
        const card = heading.closest(".card") // Находим родительский элемент .card
        const text = heading.textContent.trim().toLowerCase()

        if (text.includes(searchText)) {
          card.style.display = "block" // Показываем карточку, если заголовок содержит текст поиска
        } else {
          card.style.display = "none" // Скрываем карточку, если заголовок не содержит текст поиска
        }
      })
    })
  })

  // filter by boolean
  document.addEventListener("DOMContentLoaded", function () {
    const statusFilter = document.getElementById("statusFilter")
    const cards = document.querySelectorAll(".card")

    statusFilter.addEventListener("change", function () {
      const selectedStatus = this.value

      cards.forEach(function (card) {
        const cardStatus = card.classList.contains(selectedStatus)

        if (selectedStatus === "all" || cardStatus) {
          card.style.display = "block"
        } else {
          card.style.display = "none"
        }
      })
    })
  })

  // filter by priority
  document.addEventListener("DOMContentLoaded", function () {
    const priorityFilter = document.getElementById("priorityFilter")
    const cards = document.querySelectorAll(".card")

    priorityFilter.addEventListener("change", function () {
      const selectedPriority = this.value

      cards.forEach(function (card) {
        const cardPriority = card.getAttribute("data-priority")

        if (selectedPriority === "all" || cardPriority === selectedPriority) {
          card.style.display = "block"
        } else {
          card.style.display = "none"
        }
      })
    })
  })

  // apply all filters btn

  document.addEventListener("DOMContentLoaded", function () {
    const searchInput = document.getElementById("searchInput")
    const priorityFilter = document.getElementById("priorityFilter")
    const applyFiltersBtn = document.getElementById("applyFiltersBtn")
    const cards = document.querySelectorAll(".card")

    // Функция для применения всех фильтров
    function applyFilters() {
      const searchText = searchInput.value.trim().toLowerCase()
      const selectedPriority = priorityFilter.value

      cards.forEach(function (card) {
        const cardPriority = card.getAttribute("data-priority")
        const cardText = card
          .querySelector("h3")
          .textContent.trim()
          .toLowerCase()

        // Проверка фильтра по тексту
        const textFilter = cardText.includes(searchText)

        // Проверка фильтра по приоритету
        const priorityFilter =
          selectedPriority === "all" || cardPriority === selectedPriority

        // Показываем карточку, если соответствует всем фильтрам, иначе скрываем
        if (textFilter && priorityFilter) {
          card.style.display = "block"
        } else {
          card.style.display = "none"
        }
      })
    }

    // Обработчик нажатия кнопки "Применить фильтры"
    applyFiltersBtn.addEventListener("click", applyFilters)

    // Обновляем результаты фильтрации при изменении значений в полях поиска и фильтра по приоритету
    searchInput.addEventListener("input", applyFilters)
    priorityFilter.addEventListener("change", applyFilters)
  })
}
