export function modalWindow() {
  // modal window for enter site

  document.addEventListener("DOMContentLoaded", function () {
    const modal = document.getElementById("modal")
    const btnEnter = document.querySelector(".btn.enter")
    const btnSubmit = document.getElementById("submit")
    const closeModal = document.querySelector(".close")

    btnEnter.addEventListener("click", function () {
      modal.style.display = "block"
    })

    closeModal.addEventListener("click", function () {
      modal.style.display = "none"
    })

    btnSubmit.addEventListener("click", function () {
      const email = document.getElementById("email").value
      const password = document.getElementById("password").value
      const btnVisit = document.createElement("button")

      // Проверяем условие валидации email и пароля
      if (email === "doctor@medical.com" && password === "1111") {
        modal.style.display = "none"

        btnVisit.textContent = "Створити візит"
        btnVisit.classList.add("btn", "visit")
        btnVisit.addEventListener("click", function () {
          // Действия по клику на кнопку "Створити візит"
          console.log("Створення візиту...")
          visitModal.style.display = "block"
        })

        btnEnter.parentNode.replaceChild(btnVisit, btnEnter)
      } else {
        alert("Неправильний email або пароль")
      }
    })
  })

  // modal window for new item
  document.addEventListener("DOMContentLoaded", function () {
    const visitModal = document.getElementById("visitModal")
    const createVisitBtn = document.getElementById("createVisitBtn")
    const closeModal = visitModal.querySelector(".close")
    const additionalFields = document.getElementById("additionalFields")
    const doctorSelect = document.getElementById("doctorSelect")

    // Функція для відображення додаткових полів в залежності від вибору лікаря
    function showAdditionalFields() {
      const selectedDoctor = doctorSelect.value
      additionalFields.innerHTML = "" // Очистка попередніх полів

      if (selectedDoctor === "cardiologist") {
        // Додаткові поля для кардіолога
        additionalFields.innerHTML = `
                    <input type="text" id="bloodPressure" placeholder="Звичайний тиск">
                    <input type="text" id="bmi" placeholder="Індекс маси тіла">
                    <input type="text" id="heartDiseases" placeholder="Перенесені захворювання серцево-судинної системи">
                    <input type="text" id="age" placeholder="Вік">
                `
      } else if (selectedDoctor === "dentist") {
        // Додаткові поля для стоматолога
        additionalFields.innerHTML = `
                    <input type="date" id="lastVisitDate" placeholder="Дата останнього відвідування">
                `
      } else if (selectedDoctor === "therapist") {
        // Додаткові поля для терапевта
        additionalFields.innerHTML = `
                    <input type="text" id="age" placeholder="Вік">
                `
      }
    }

    // Відображення модального вікна при кліку на кнопку "Створити візит"
    createVisitBtn.addEventListener("click", function () {
      visitModal.style.display = "block"
    })

    // Закриття модального вікна при кліку на "Закрити" або поза вікном
    closeModal.addEventListener("click", function () {
      visitModal.style.display = "none"
    })

    // Відображення додаткових полів при зміні обраного лікаря
    doctorSelect.addEventListener("change", showAdditionalFields)
  })

  window.addEventListener("click", function (event) {
    if (event.target === modal) {
      modal.style.display = "none"
    }
  })

  // Закриття модального вікна "Створити візит" при кліку поза ним
  window.addEventListener("click", function (event) {
    if (event.target === visitModal) {
      visitModal.style.display = "none"
    }
  })
}
